# iTunesToMP3

This is a script to help you migrate away from iTunes, to a more conventional and open mp3-based system.
It was originally developed and tested on a diverse 15k song library.

## Features ##

* Automatic transcoding of common media formats to `.mp3`
* Warnings for `.m3p` files, which are protected by DRM, and support for placing `.mp3` file alternatives manually
* iTunes rating and play counts converted to the standardised mp3 id3v2-based Popularimeter format (supported by Clementine and RhythmboxPOPM)
* Converts playlists to `.m3u` format
* Deals with case insensitivity problems in iTunes library paths
* Supports Unicode filenames, fixing Unicode-normalisation problems with iTunes`-own-encoding
* Very robust to file errors

## Philosophy ##

There are at least a couple of other Open Source iTunes migration tools, but I found they either would not compile anymore, or gave segmentation faults when tagging some mp3 files (probably partly-corrupted ones). While I could have tried to fix those tools, I think philosophically a script-based approach is more better - easy to tweak, and calling separate helper tools rather than libraries means segmentation faults are isolated.

## System requirements ##

Currently our script is tested only on Linux (Ubuntu 19 to be specific). Contributions to expand support are welcome.

Dependencies:
* ffmpeg/avconf
* mid3v2 (Mutagen)
* PHP
* PHP intl extension.

## Usage ##

First, make sure you have pre-created `.mp3`-equivalents to any DRM-protected `.m4p` files. You bought them, so you probably will feel justified getting them off YouTube using something like "YouTube-DL GUI". If you have a file `example.m4p`, you should have a matching file `example.mp3` in the same directory. By doing this your ratings, play counts, and playlists, will automatically carry over correctly to the matching `.mp3` files.

The script will look for the standard-named iTunes library XML file, in the current working directory. It will expect to find the media files underneath the same directory, and is smart enough to do automatic prefix stripping to generate relative file paths (iTunes works with absolute file paths, which are unlikely to be correct on your Linux box).

You want to do a dry-run and look for warnings. To do this edit `iTunesToMP3.php` in a text editor, setting `$dry_run = true;`. Do a dry-run looking at warnings with:
```
php iTunesToMP3.php you@example.com | grep "WARNING:"
```

If you see things that concern you, do whatever you need to resolve them (e.g. finding missing files, fixing incorrect file path in the iTunes XML file, or removing dead files you didn't even know were in your library). The script is error tolerant, but by fixing as much upfront as possible you'll get a better set of playlists generated.

When you are ready and `$dry_run = false;` is back, run with:
```
php iTunesToMP3.php you@example.com
```

I recommend to remove pre-transcoding media files that aren't needed anymore (I'll assume you've got sensible backups already, if not make sure you do):

```
find . -iname "*.m3p" -exec rm {} \;
find . -iname "*.m3a" -exec rm {} \;
find . -iname "*.wav" -exec rm {} \;
find . -iname "*.aac" -exec rm {} \;
find . -iname "*.mp2" -exec rm {} \;
```

You may want to rearrange your folders a bit to remove the complicated iTunes folder structure. This is fine as long as you edit the auto-generated `.m3u` playlist files to match your changes (some simple search and replace work).

That's it, enjoy your iTunes-free life.
