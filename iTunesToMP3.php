<?php

/**
 * iTunesToMP3
 *
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

$dry_run = false;

ini_set('display_errors', '1');
error_reporting(E_ALL);

if ((!isset($_SERVER['argv'][1])) || (strpos($_SERVER['argv'][1], '@') === false)) {
    throw new Exception('Must pass an e-mail address as a parameter; this is what ratings will be saved as being from');
}
$email_address = $_SERVER['argv'][1];

$library_path = 'iTunes Music Library.xml';
if (!is_file($library_path)) {
    throw new Exception('Could not find "iTunes Music Library.xml" -- you must run from the same directory as the iTunes XML file. If you do not have an up-to-date one, create one from iTunes (File > Library > Export Library).');
}

if (strpos(shell_exec('mid3v2 --help 2>&1'), 'Mutagen') === false) {
    throw new Exception('Could not find mid3v2 (python-mutagen), make sure it is installed and in the path');
}

if (strpos(shell_exec('ffmpeg --help 2>&1'), 'usage: ') === false) {
    $coder = 'avconv';
    if (strpos(shell_exec('avconv --help 2>&1'), 'usage: ') === false) {
        throw new Exception('Could not find ffmpeg or avconv, make sure it is installed and in the path');
    }
} else {
    $coder = 'ffmpeg';
}

$is_windows = (strcasecmp(substr(PHP_OS, 0, 3), 'WIN') == 0);
if ($is_windows) {
    throw new Exception('This script is not tested under Windows and almost certainly will fail badly');
}

if (!function_exists('normalizer_normalize')) {
    throw new Exception('PHP intl extension is required');
}

$c = file_get_contents($library_path);

$c = str_replace("\n\t\t</dict>", "\n\t\t</dict>" . str_repeat(' ', 2000), $c); // HACKHACK: Stop regexps bleeding between when not properly matching

// Finding individual songs
$regexp = '#\n\t\t<dict>\s*<key>Track ID</key>\s*<integer>(\d*)</integer>.{0,2000}<key>Location</key>\s*<string>([^<>]*)</string>.{0,2000}</dict>#Us';
$num_matches = preg_match_all($regexp, $c, $matches);
if ($num_matches == 0) {
    throw new Exception('No songs found in iTunes Music Library.xml file');
}
$path_stem = null;
$library = array();
for ($i = 0; $i < $num_matches; $i++) {
    $track_id = $matches[1][$i];

    $url = html_entity_decode($matches[2][$i], ENT_QUOTES, 'utf-8');
    if (preg_match('#^file://#', $url) != 0) {
        $path = rawurldecode(substr($url, strlen('file://')));
        $path = preg_replace('#^[A-Z]:#', '', str_replace('\\', '/', $path)); // Remove Windows crap

        if ($path_stem === null) {
            $pos = 0;
            do {
                $end = substr($path, $pos + 1);
                if (is_file_hackhack($end)) {
                    $path_stem = substr($path, 0, $pos + 1);
                    output_info('Found that we need to strip the following stems off our file paths: ' . $path_stem);
                    break;
                }
                $pos = strpos($path, '/', $pos + 1);
            }
            while ($pos !== false);
            if ($pos === false) {
                throw new Exception('Failed to work out file path stems, likely music files are not underneath the current working directory in a file structure that is a substructure of the paths in the iTunes Music Library.xml file');
            }
        }
    } else {
        output_warning('Weird URL in playlist: ' . $url . ' (skipped)');
        continue;
    }
    if (substr($path, 0, strlen($path_stem)) == $path_stem) {
        $path = substr($path, strlen($path_stem));
    }

    if (is_file_hackhack($path)) {
        output_info('Found "' . $path . '" (#' . $track_id . ')');
    } else {
        output_warning('Missing file "' . $path . '" (#' . $track_id . ')');
        continue;
    }

    $rating = null;
    $matches2 = array();
    if (preg_match('#<key>Rating</key>\s*<integer>(\d+)</integer>#Us', $matches[0][$i], $matches2) != 0) {
        $rating = $matches2[1];
    }

    $play_count = '0';
    $matches2 = array();
    if (preg_match('#<key>Play Count</key>\s*<integer>(\d+)</integer>#Us', $matches[0][$i], $matches2) != 0) {
        $play_count = $matches2[1];
    }

    $library[$track_id] = array(
        'path' => $path,
        'play_count' => $play_count,
        'rating' => $rating,
    );
}

// Converting file formats
$removed = array();
$transcodable_formats = array('.m4a', '.mp2', '.aac', '.wav');
foreach ($library as $track_id => &$track) {
    $path = $track['path'];

    $ext = strtolower(substr($path, -4));
    if ($ext != '.mp3') {
        $mp3_path = substr($path, 0, strlen($path) - 4) . '.mp3';
        if (file_exists($mp3_path)) {
            output_info('Transcoding "' . $path . '" to "' . $mp3_path . '" skipped - already transcoded');
            $track['path'] = $mp3_path;
        } else {
            if (in_array($ext, $transcodable_formats)) {
                $command = $coder . ' -i ' . escapeshellarg($path) . ' -b:a 192K -vn ' . escapeshellarg($mp3_path);
                output_info('Running transcoding command... "' . $command . '"');
                if ($dry_run) {
                    output_info('(Dry run, skipping)');
                } else {
                    echo shell_exec($command);

                    if ((is_file_hackhack($mp3_path)) && ((float)filesize($mp3_path) > (float)filesize($path) / 10.0)) {
                        output_info('Transcoding "' . $path . '" to "' . $mp3_path . '" successful; you can delete the .' . $ext . ' file if you like');
                        $track['path'] = $mp3_path;
                    } else {
                        output_warning('Transcoding "' . $path . '" to "' . $mp3_path . '" failed - will keep original file in m3u playlists but without any id3 tags');
                    }
                }
            } elseif ($ext == '.m4p') {
                output_warning('Cannot convert "' . $path . '" due to presumed iTunes DRM');
                $removed[$track_id] = $track;
                unset($library[$track_id]);
            } else {
                output_warning('Could not recognise file format of "' . $path . '"');
                $removed[$track_id] = $track;
                unset($library[$track_id]);
            }
        }
    }
}

// Adding tagging
foreach ($library as $track_id => &$track) {
    $path = $track['path'];

    $ext = strtolower(substr($path, -4));
    if ($ext == '.mp3') {
        if ($track['rating'] === null) {
            $normalised_rating = 0;
        } else {
            $normalised_rating = strval(round($track['rating'] * 255.0 / 100.0));
        }
        $command = 'mid3v2 --POPM ' . escapeshellarg($email_address . ':' . strval($normalised_rating) . ':' . $track['play_count']) . ' ' . escapeshellarg($path);
        output_info('Running id3 command... "' . $command . '"');
        if ($dry_run) {
            output_info('(Dry run, skipping)');
        } else {
            echo shell_exec($command);
        }
    } else {
        if ((!$dry_run) || (!in_array($ext, $transcodable_formats))) {
            output_warning('Will not try to add id3 to a non-.mp3 file, "' . $path . '"');
        }
    }
}

// Creating playlists
ini_set('pcre.backtrack_limit', '10000000');
ini_set('pcre.recursion_limit', '10000000');
$num_matches = preg_match_all('#\n\t\t<dict>.{0,1000}<key>Playlist ID</key>\s*<integer>(\d+)</integer>.{0,1000}<key>Name</key>\s*<string>([^<>]+)</string>.{0,1000}<array>(.*)</array>#Us', $c, $matches);
output_info('Found ' . strval($num_matches) . ' playlists');
$playlists = array();
for ($i = 0; $i < $num_matches; $i++) {
    $playlist_id = $matches[1][$i];
    $playlist_name = $matches[2][$i];

    if (in_array($playlist_name, array('_', 'Podcasts', 'Genius', 'Library', 'Music', 'Recently Played', 'Recently Added', 'TV Shows'))) {
        output_info('Skipped inbuilt playlist: ' . $playlist_name);
        continue;
    }

    if (isset($playlists[$playlist_name])) {
        output_warning('Duplicate playlist, skipping: ' . $playlist_name);
        continue;
    } else {
        output_info('Found playlist: ' . $playlist_name);
    }

    $playlist_contents = array();
    $matches2 = array();
    $num_matches2 = preg_match_all('#<dict>\s*<key>Track ID</key>\s*<integer>(\d+)</integer>\s*</dict>#Us', $matches[3][$i], $matches2);
    for ($j = 0; $j < $num_matches2; $j++) {
        $track_id = $matches2[1][$j];

        if (isset($removed[$track_id])) {
            $playlist_contents[] = $removed[$track_id]['path'];
            continue;
        }

        if (isset($library[$track_id])) {
            $playlist_contents[] = $library[$track_id]['path'];
            continue;
        }

        output_warning('Could not find track #' . $track_id . ', referenced in playlist #' . $playlist_id . ' (' . $playlist_name . ')');
    }

    if (count($playlist_contents) > 0) {
        $playlists[$playlist_name] = $playlist_contents;
    } else {
        output_warning('(Empty playlist)');
    }
}
foreach ($playlists as $playlist_name => $playlist_contents) {
    $m3u_code = '';
    foreach ($playlist_contents as $path) {
        $m3u_code .= $path . "\n";
    }

    if ($is_windows) {
        $sanitised_name = str_replace(array('<', '>', ':', '"', '/', '\\', '|', '?', '*'), array('(', ')', ';', '', '', '_', '_', '', '_'), $playlist_name);
    } else {
        $sanitised_name = str_replace('/', '\\', $playlist_name);
    }

    file_put_contents($sanitised_name . '.m3u', $m3u_code);
}

function output_warning($msg)
{
    echo 'WARNING: ' . $msg . "\n";
}

function output_info($msg)
{
    echo 'INFO: ' . $msg . "\n";
}

function is_file_hackhack(&$path)
{
    $path = normalizer_normalize($path, Normalizer::FORM_C);

    if (is_file($path)) {
        // Simple case
        return true;
    }

    // PHP does not support utf-8 filenames natively, so we need to use the shell :(
    $command = 'ls --literal ' . escapeshellarg($path) . ' 2>&1';
    $result = shell_exec($command);
    if (strpos($result, 'ls:') !== false) {
        // Plus this lets us fix case sensitivity problems
        $command = 'bash -O nocaseglob -c ' . escapeshellarg('ls --literal ' . str_replace('/', "'*/'", escapeshellarg($path)) . ' 2>&1');
        $result = exec($command);
        if (strpos($result, 'ls:') === false) {
            output_info('Fixed case sensitivity problem, "' . $path . '" ==> "' . trim($result) . '"');
        }
    }
    if (strpos($result, 'ls:') !== false) {
        return false;
    }
    $path = trim($result);
    return true;
}
